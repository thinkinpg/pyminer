pyminer2.ui.generalwidgets.textctrls package
============================================

Submodules
----------

pyminer2.ui.generalwidgets.textctrls.highlighter module
-------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.highlighter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.textctrls.textctrl module
----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.textctrl
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.textctrls.textwidgetlineshow module
--------------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.textwidgetlineshow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls
   :members:
   :undoc-members:
   :show-inheritance:
