pmgwidgets package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pmgwidgets.normal
   pmgwidgets.table

Module contents
---------------

.. automodule:: pmgwidgets
   :members:
   :undoc-members:
   :show-inheritance:
