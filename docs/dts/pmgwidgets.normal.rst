pmgwidgets.normal package
=========================

Submodules
----------

pmgwidgets.normal.value\_inputs module
--------------------------------------

.. automodule:: pmgwidgets.normal.value_inputs
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pmgwidgets.normal
   :members:
   :undoc-members:
   :show-inheritance:
