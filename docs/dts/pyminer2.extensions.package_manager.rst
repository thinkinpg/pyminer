pyminer2.extensions.package\_manager package
============================================

Submodules
----------

pyminer2.extensions.package\_manager.env\_manager module
--------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.env_manager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_install module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_install
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_manager module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_manager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_remove module
-----------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_remove
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_setting module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_setting
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_update module
-----------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_update
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.package_manager
   :members:
   :undoc-members:
   :show-inheritance:
