API Reference
=================


app2 module
===========

.. automodule:: app2
   :members:
   :undoc-members:
   :show-inheritance:
pyminer
=======

.. toctree::
   :maxdepth: 4

   app2
   pmgwidgets
   pyminer2
pmgwidgets.normal package
=========================

Submodules
----------

pmgwidgets.normal.value\_inputs module
--------------------------------------

.. automodule:: pmgwidgets.normal.value_inputs
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pmgwidgets.normal
   :members:
   :undoc-members:
   :show-inheritance:
pmgwidgets package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pmgwidgets.normal
   pmgwidgets.table

Module contents
---------------

.. automodule:: pmgwidgets
   :members:
   :undoc-members:
   :show-inheritance:
pmgwidgets.table package
========================

Submodules
----------

pmgwidgets.table.tableviews module
----------------------------------

.. automodule:: pmgwidgets.table.tableviews
   :members:
   :undoc-members:
   :show-inheritance:

pmgwidgets.table.tablewidgets module
------------------------------------

.. automodule:: pmgwidgets.table.tablewidgets
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pmgwidgets.table
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.extensionlib package
========================================

Submodules
----------

pyminer2.extensions.extensionlib.baseext module
-----------------------------------------------

.. automodule:: pyminer2.extensions.extensionlib.baseext
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensionlib.extension\_lib module
------------------------------------------------------

.. automodule:: pyminer2.extensions.extensionlib.extension_lib
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensionlib.pmext module
---------------------------------------------

.. automodule:: pyminer2.extensions.extensionlib.pmext
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.extensionlib
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.extensions\_manager package
===============================================

Submodules
----------

pyminer2.extensions.extensions\_manager.ExtensionLoader module
--------------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.ExtensionLoader
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.UIInserter module
---------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.UIInserter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.log module
--------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.log
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.manager module
------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.manager
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.extensions_manager
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.package\_manager package
============================================

Submodules
----------

pyminer2.extensions.package\_manager.env\_manager module
--------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.env_manager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_install module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_install
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_manager module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_manager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_remove module
-----------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_remove
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_setting module
------------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_setting
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.package\_manager.package\_update module
-----------------------------------------------------------

.. automodule:: pyminer2.extensions.package_manager.package_update
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.package_manager
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions package
===========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.extensionlib
   pyminer2.extensions.extensions_manager
   pyminer2.extensions.package_manager
   pyminer2.extensions.test_demo

Module contents
---------------

.. automodule:: pyminer2.extensions
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.test\_demo.extensions package
=================================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.test_demo.extensions.test_extension
   pyminer2.extensions.test_demo.extensions.test_extension2

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.test\_demo.extensions.test\_extension2 package
==================================================================

Submodules
----------

pyminer2.extensions.test\_demo.extensions.test\_extension2.entry module
-----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.entry
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.interface module
---------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.interface
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.menu module
----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.menu
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.subwindow module
---------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.subwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.test\_demo.extensions.test\_extension package
=================================================================

Submodules
----------

pyminer2.extensions.test\_demo.extensions.test\_extension.entry module
----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.entry
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.interface module
--------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.interface
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.menu module
---------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.menu
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.subwindow module
--------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.subwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.extensions.test\_demo package
======================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.test_demo.extensions

Submodules
----------

pyminer2.extensions.test\_demo.extensionlib module
--------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensionlib
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.mainform module
----------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.mainform
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2 package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions
   pyminer2.ui
   pyminer2.workspace

Submodules
----------

pyminer2.pmappmodern module
---------------------------

.. automodule:: pyminer2.pmappmodern
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.pmutil module
----------------------

.. automodule:: pyminer2.pmutil
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.base package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.base.widgets

Submodules
----------

pyminer2.ui.base.aboutMe module
-------------------------------

.. automodule:: pyminer2.ui.base.aboutMe
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.flow module
----------------------------

.. automodule:: pyminer2.ui.base.flow
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.mainForm module
--------------------------------

.. automodule:: pyminer2.ui.base.mainForm
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.newItem module
-------------------------------

.. automodule:: pyminer2.ui.base.newItem
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.option module
------------------------------

.. automodule:: pyminer2.ui.base.option
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.base
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.base.widgets package
================================

Submodules
----------

pyminer2.ui.base.widgets.consolewidget module
---------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.consolewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.controlpanel module
--------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.controlpanel
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.flowwidget module
------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.flowwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.menu\_tool\_stat\_bars module
------------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.menu_tool_stat_bars
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.notificationwidget module
--------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.notificationwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.reportwidget module
--------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.reportwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.resources module
-----------------------------------------

.. automodule:: pyminer2.ui.base.widgets.resources
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.tablewidget module
-------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.tablewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.texteditconsolewidget module
-----------------------------------------------------

.. automodule:: pyminer2.ui.base.widgets.texteditconsolewidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.base.widgets.treeviews module
-----------------------------------------

.. automodule:: pyminer2.ui.base.widgets.treeviews
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.base.widgets
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.common package
==========================

Submodules
----------

pyminer2.ui.common.locale module
--------------------------------

.. automodule:: pyminer2.ui.common.locale
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.common.openprocess module
-------------------------------------

.. automodule:: pyminer2.ui.common.openprocess
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.common.platformutil module
--------------------------------------

.. automodule:: pyminer2.ui.common.platformutil
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.common.test\_comm module
------------------------------------

.. automodule:: pyminer2.ui.common.test_comm
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.common
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.basicwidgets package
===============================================

Submodules
----------

pyminer2.ui.generalwidgets.basicwidgets.labels module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.basicwidgets.labels
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.basicwidgets.object module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.basicwidgets.object
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.basicwidgets.pmpushbuttons module
------------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.basicwidgets.pmpushbuttons
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.basicwidgets.toolbutton module
---------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.basicwidgets.toolbutton
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.basicwidgets
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.browser package
==========================================

Submodules
----------

pyminer2.ui.generalwidgets.browser.browser module
-------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.browser.browser
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.browser
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.containers package
=============================================

Submodules
----------

pyminer2.ui.generalwidgets.containers.PMTab module
--------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.PMTab
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.containers.flowarea module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.flowarea
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.containers.pmscrollarea module
---------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.pmscrollarea
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.containers
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.demos\_and\_tests package
====================================================

Submodules
----------

pyminer2.ui.generalwidgets.demos\_and\_tests.qmenu\_demo module
---------------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.demos_and_tests.qmenu_demo
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.demos_and_tests
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.layouts package
==========================================

Submodules
----------

pyminer2.ui.generalwidgets.layouts.flowlayout module
----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.layouts.flowlayout
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.layouts
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets package
==================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.generalwidgets.basicwidgets
   pyminer2.ui.generalwidgets.browser
   pyminer2.ui.generalwidgets.containers
   pyminer2.ui.generalwidgets.demos_and_tests
   pyminer2.ui.generalwidgets.layouts
   pyminer2.ui.generalwidgets.sourcemgr
   pyminer2.ui.generalwidgets.table
   pyminer2.ui.generalwidgets.textctrls
   pyminer2.ui.generalwidgets.toolbars
   pyminer2.ui.generalwidgets.window

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.sourcemgr package
============================================

Submodules
----------

pyminer2.ui.generalwidgets.sourcemgr.iconutils module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.sourcemgr.iconutils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.sourcemgr
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.table package
========================================

Submodules
----------

pyminer2.ui.generalwidgets.table.tableviews module
--------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.table.tableviews
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.table.tablewidgets module
----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.table.tablewidgets
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.table
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.textctrls package
============================================

Submodules
----------

pyminer2.ui.generalwidgets.textctrls.highlighter module
-------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.highlighter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.textctrls.textctrl module
----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.textctrl
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.textctrls.textwidgetlineshow module
--------------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls.textwidgetlineshow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.textctrls
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.toolbars package
===========================================

Submodules
----------

pyminer2.ui.generalwidgets.toolbars.toolbar module
--------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.toolbars.toolbar
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.toolbars
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.generalwidgets.window package
=========================================

Submodules
----------

pyminer2.ui.generalwidgets.window.applicationtest module
--------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.applicationtest
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.mainwindow\_new module
--------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.mainwindow_new
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.pmdockwidget module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.pmdockwidget
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.window.pmmainwindow module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.window.pmmainwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.window
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.pmwidgets package
=============================

Submodules
----------

pyminer2.ui.pmwidgets.toplevel module
-------------------------------------

.. automodule:: pyminer2.ui.pmwidgets.toplevel
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.pmwidgets
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.base
   pyminer2.ui.common
   pyminer2.ui.generalwidgets
   pyminer2.ui.pmwidgets
   pyminer2.ui.source

Submodules
----------

pyminer2.ui.pyqtsource\_rc module
---------------------------------

.. automodule:: pyminer2.ui.pyqtsource_rc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.source.qss package
==============================

Submodules
----------

pyminer2.ui.source.qss.qss\_tools module
----------------------------------------

.. automodule:: pyminer2.ui.source.qss.qss_tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.source.qss
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.ui.source package
==========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.source.qss

Module contents
---------------

.. automodule:: pyminer2.ui.source
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.workspace.datamanager package
======================================

Submodules
----------

pyminer2.workspace.datamanager.converter module
-----------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.converter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.datamanager module
-------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.datamanager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.dataset module
---------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.dataset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.exceptions module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.historyset module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.historyset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.metadataset module
-------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.metadataset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.recyclebin module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.recyclebin
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.variable module
----------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.variable
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.varset module
--------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.varset
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.workspace.datamanager
   :members:
   :undoc-members:
   :show-inheritance:
pyminer2.workspace package
==========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.workspace.datamanager

Module contents
---------------

.. automodule:: pyminer2.workspace
   :members:
   :undoc-members:
   :show-inheritance:
