#!/bin/bash

cd docs 

sphinx-apidoc -o ./dts/ ..

touch lib_ref.rst
 
echo -e "API Reference\n=================\n\n" >> lib_ref.rst


for file in ./dts/*
do
    cat $file >> lib_ref.rst
done

mv lib_ref.rst ./user

make html